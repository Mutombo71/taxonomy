Groups and Standings

    Group A
        Germany: 3 matches, 6 goal difference, 7 points
        Switzerland: 3 matches, 2 goal difference, 5 points
        Hungary: 3 matches, -3 goal difference, 3 points
        Scotland: 3 matches, -5 goal difference, 1 point

    Group B
        Spain: 3 matches, 5 goal difference, 9 points
        Italy: 3 matches, 0 goal difference, 4 points
        Croatia: 3 matches, -3 goal difference, 2 points
        Albania: 3 matches, -2 goal difference, 1 point

    Group C
        England: 3 matches, 1 goal difference, 5 points
        Denmark: 3 matches, 0 goal difference, 3 points
        Slovenia: 3 matches, 0 goal difference, 3 points
        Serbia: 3 matches, -1 goal difference, 2 points

    Group D
        Austria: 3 matches, 2 goal difference, 6 points
        France: 3 matches, 1 goal difference, 5 points
        Netherlands: 3 matches, 0 goal difference, 4 points
        Poland: 3 matches, -3 goal difference, 1 point

    Group E
        Romania: 2 matches, 1 goal difference, 3 points
        Belgium: 2 matches, 1 goal difference, 3 points
        Slovakia: 2 matches, 0 goal difference, 3 points
        Ukraine: 2 matches, -2 goal difference, 3 points

    Group F
        Portugal: 2 matches, 4 goal difference, 6 points
        Türkiye: 2 matches, -1 goal difference, 3 points
        Czechia: 2 matches, -1 goal difference, 1 point
        Georgia: 2 matches, -2 goal difference, 1 point

Highlights

    England tops Group C, Slovenia also qualifies1.
    Denmark reaches the last 161.
    Austria advances in style1.
    Poland pegs back France1.
    Player of The Match: Adam Gnezda Čerin1.
    Player of The Match: Christian Eriksen1.
    Player of The Match: Marcel Sabitzer1.
    Player of The Match: Łukasz Skorupski1.

The UEFA EURO 2024 Championship is taking place across ten cities in Germany1. Here are the host cities along with their respective stadiums:

    Berlin: Olympiastadion Berlin (Capacity: 71,000)2
    Cologne: Cologne Stadium (Capacity: 43,000)2
    Dortmund: BVB Stadion Dortmund (Capacity: 62,000)2
    Düsseldorf: Düsseldorf Arena (Capacity: 47,000)2
    Frankfurt: Frankfurt Arena (Capacity: 47,000)2
    Gelsenkirchen: Arena AufSchalke (Capacity: 50,000)2
    Hamburg: Volksparkstadion Hamburg (Capacity: 49,000)2
    Leipzig: Leipzig Stadium (Capacity: 40,000)2
    Munich: Munich Football Arena (Capacity: 66,000)2
    Stuttgart: Stuttgart Arena (Capacity: 51,000)2

